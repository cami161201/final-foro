import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PublicacionService } from 'src/app/services/publicacion.service';

@Component({
  selector: 'app-agregar-publi',
  templateUrl: './agregar-publi.component.html',
  styleUrls: ['./agregar-publi.component.css']
})
export class AgregarPubliComponent implements OnInit {
post:FormGroup;
  constructor(private _postService:PublicacionService) {
    this.post=new FormGroup({
      titulo:new FormControl('',[Validators.required,Validators.minLength(4)]),
      categoria:new FormControl('',Validators.required),
      descripcion:new FormControl('',[Validators.required,Validators.minLength(4)])
    })
   }

  ngOnInit(): void {
  }

  async addPost(){
    console.log(this.post.value);
    const response= await this._postService.addPost(this.post.value);
    console.log(response);
    
  }

  get titulo(){
    return this.post.get('titulo');

  }
  get descripcion(){
    return this.post.get('descripcion');
  }

}
