import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IComentario } from 'src/app/models/comentario';
import { UsersService } from 'src/app/services/users.service';
import { ExtrasService } from 'src/app/services/extras.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatCard } from '@angular/material/card';
import { IPublicacion } from 'src/app/models/publicacion.interface';
import { PublicacionService } from 'src/app/services/publicacion.service';
import {MatDialog} from '@angular/material/dialog';
import { AgregarPubliComponent } from '../agregar-publi/agregar-publi.component';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
})
export class InicioComponent implements OnInit {
  post:IPublicacion[]=[];
  com:IComentario[]=[];
  come2:IComentario[]=[];
  panelOpenState = false;
  form!: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private comenService: ExtrasService,
    private _snackbar:MatSnackBar,
    private _publicacionService:PublicacionService,
    private usersService:UsersService,
    public dialog: MatDialog,
  ) {}

  user$ = this.usersService.currentUserProfile$;
  ngOnInit(): void {
    this.com= this.comenService.obtenerComentario();
    this.come2=this.comenService.obtenerComentario();
    this._publicacionService.obtenerPost().subscribe(post=>
      this.post = post)
  }
  openDialog() {
    const dialogRef = this.dialog.open(AgregarPubliComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
 
COMENTARIOS:IComentario[] = [];
datos!:MatCard;

cargarComentario(){
this.COMENTARIOS = this.comenService.obtenerComentario();
this.datos=new MatCard;
}

  agregarComentarios(comentario: HTMLInputElement) {
    this.comenService.agregarComentarios({
      mensaje: comentario.value,
    });
    console.log(this.comenService.obtenerComentario());

    return false;
  }

  borrarComentario(comentario:IComentario){
 
      this.comenService.eliminarComentario(comentario);
     
      
    }

 
}


