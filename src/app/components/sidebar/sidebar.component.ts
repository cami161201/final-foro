import { Component, OnInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { delay, filter } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
@UntilDestroy()
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  constructor(private observer: BreakpointObserver, 
    private router: Router,
    private _formBuilder: FormBuilder) {}
  ngOnInit(): void {

  }

  ngAfterViewInit():void {
    this.observer
      .observe(['(max-width: 800px)'])
      .pipe(delay(1), untilDestroyed(this))
      .subscribe((res) => {
        if (res.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.close();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });

    this.router.events
      .pipe(
        untilDestroyed(this),
        filter((e) => e instanceof NavigationEnd)
      )
      .subscribe(() => {
        if (this.sidenav.mode === 'over') {
          this.sidenav.close();
        }
      });
  }

  primeroFormGroup = this._formBuilder.group({
    primeroCtrl: ['', Validators.required],
  });
  segundoFormGroup = this._formBuilder.group({
    segundoCtrl: ['', Validators.required],
  });
  terceroFormGroup = this._formBuilder.group({
    tercerCtrl: ['', Validators.required],
  });
  cuartoFormGroup = this._formBuilder.group({
    cuartoCtrl: ['', Validators.required],
  });
  quintoFormGroup = this._formBuilder.group({
    quintoCtrl: ['', Validators.required],
  });
}
