import { Injectable } from '@angular/core';
import { Firestore, addDoc, collectionData} from '@angular/fire/firestore';
import { collection,doc,deleteDoc } from '@firebase/firestore';
import { Observable } from 'rxjs';
import { IPublicacion } from '../models/publicacion.interface';
import { ProfileUser } from '../models/user';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class PublicacionService {
  post:IPublicacion[]=[];
  
  constructor(private firestore:Firestore,private usersService:UsersService) {
    this.post=[
      {titulo:'Italia',
      categoria:'Arte',
      descripcion:'Italia, país europeo con una larga costa mediterránea, influyó considerablemente en la cultura y la cocina occidental. Su capital, Roma, es hogar del Vaticano, de ruinas antiguas y de obras de arte emblemáticas.'},

    ];
  }

  obtenerPost():Observable<IPublicacion[]>{
    const postRef = collection(this.firestore,'publicaciones');
    return collectionData(postRef,{idField:'id'})as Observable<IPublicacion[]>;
  }

  addPost(publicacion:IPublicacion){
    const postRef = collection(this.firestore,'publicaciones');
    return addDoc(postRef,publicacion);
  }



  deletePost(publicacion:IPublicacion){
    const postDocRef = doc(this.firestore,`publicaciones/${publicacion.id}`);
    return deleteDoc(postDocRef);
  }
}
