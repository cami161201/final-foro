import { Injectable } from '@angular/core';
import { IComentario } from '../models/comentario';

@Injectable({
  providedIn: 'root'
})
export class ExtrasService {
  comentarios:IComentario[] = [];

  constructor() { 
    this.comentarios=[     
     ]
  }

  obtenerComentario(){
    if(localStorage.getItem('coment')===null){
      return this.comentarios;
    }else{
      this.comentarios=JSON.parse(localStorage.getItem('coment')||"[]")
      return this.comentarios
    }
  }

  agregarComentarios(comentario:IComentario){
    this.comentarios.push(comentario);
    let coment: IComentario[]=[];
    if(localStorage.getItem('coment')===null){
      coment.push(comentario);
      localStorage.setItem('coment',JSON.stringify(coment));
    }else{
      coment = JSON.parse(localStorage.getItem('coment')||"[]");
      coment.push(comentario)
      localStorage.setItem('coment',JSON.stringify(coment));
    }
   
  }

  eliminarComentario(comentario:IComentario){
    for(let i = 0;i<this.comentarios.length;i++){
      if(comentario==this.comentarios[i]){
        this.comentarios.splice(i,1);
        localStorage.setItem('coment',JSON.stringify(this.comentarios))
      }
    }
  }  


  
  // modificarCita(comentario:IComentario){
  //   this.eliminarComentario(comentario.mensaje);
  //   this.agregarComentarios(comentario);
  // }





}
