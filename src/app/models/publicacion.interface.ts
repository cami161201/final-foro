export interface IPublicacion{
  id?:string;
  titulo:string;
  categoria:string;
  descripcion:string;
}